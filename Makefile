# Define the name of the binary.
NAME = "delta"

# Define Go commands.
GOCMD=go
GOBUILD=$(GOCMD) build
GORUN=$(GOCMD) run
GOMOD=$(GOCMD) mod
GOFMT=$(GOCMD) fmt
GOTEST=$(GOCMD) test
GOTOOL=$(GOCMD) tool

################################################################################
#                                                                              #
#                                  CHECKING                                    #
#                                                                              #
################################################################################

.PHONY: checking
checking: identify ensure

.PHONY: identify
identify:
	@printf "system running on : "
	@if [ "$(shell uname)" = "Darwin" ]; then \
		printf "macOS\n"; \
	elif [ -f /etc/os-release ]; then \
		if grep -q "ID=ubuntu" /etc/os-release; then \
			printf "Ubuntu\n"; \
		elif grep -q "ID=debian" /etc/os-release; then \
			printf "Debian\n"; \
		elif grep -q "ID=fedora" /etc/os-release; then \
			printf "Fedora\n"; \
		else \
			printf "Linux\n"; \
		fi \
	elif [ "$(shell uname)" = "FreeBSD" ]; then \
		printf "FreeBSD\n"; \
	elif [ "$(shell uname)" = "OpenBSD" ]; then \
		printf "OpenBSD\n"; \
	elif [ "$(shell uname)" = "NetBSD" ]; then \
		printf "NetBSD\n"; \
	elif [ "$(shell uname)" = "DragonFly" ]; then \
		printf "DragonFly BSD\n"; \
	else \
		printf "Unsupported operating system"; \
		exit 1; \
	fi

.PHONY: ensure
ensure: ensure_go ensure_golangcilint #ensure_protoc

.PHONY: ensure_go
ensure_go:
	@printf "golang installed : "
	@if ! type go >/dev/null 2>&1; then \
		printf "\033[0;31mno\033[0m\n"; \
		echo "Go is not installed on your system. Installing..."; \
		curl -o /tmp/go1.21.3.$(shell uname -s | tr '[:upper:]' '[:lower:]')-amd64.tar.gz \
		$(GO_REL)/go1.21.3.$(shell uname -s | tr '[:upper:]' '[:lower:]')-amd64.tar.gz; \
		tar -xvf /tmp/go1.21.3.$(shell uname -s | tr '[:upper:]' '[:lower:]')-amd64.tar.gz; \
		sudo chown -R root:root ./go; \
		sudo mv go /usr/local; \
		export PATH=$PATH:/usr/local/go/bin; \
		rm /tmp/go1.21.3.$(shell uname -s | tr '[:upper:]' '[:lower:]')-amd64.tar.gz; \
	else \
		printf "\033[0;32myes\033[0m\n"; \
	fi

.PHONY: ensure_golangcilint
ensure_golangcilint:
	@printf "golangci-lint installed : "
	@if ! type golangci-lint >/dev/null 2>&1; then \
		printf "\033[0;31mno\033[0m\n"; \
		echo "golangci-lint is not installed on your system. Installing..."; \
		curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | \
		sh -s -- -b $(go env GOPATH)/bin v1.55.2; \
	else \
		printf "\033[0;32myes\033[0m\n"; \
	fi

.PHONY: ensure_protoc
ensure_protoc:
	@if [ ! -z "$(PROTO_FILES)" ]; then \
		if ! type protoc >/dev/null 2>&1; then \
			echo "Protoc is not installed on your system. Installing..."; \
			if [ "$(shell uname)" = "Darwin" ]; then \
				curl -LO $(PB_REL)/download/v3.15.6/protoc-3.15.6-osx-x86_64.zip; \
				unzip protoc-3.15.6-osx-x86_64.zip -d protoc3; \
			else \
				curl -LO $(PB_REL)/download/v3.15.6/protoc-3.15.6-linux-x86_64.zip; \
				unzip protoc-3.15.6-linux-x86_64.zip -d protoc3; \
			fi \
			sudo mv protoc3/bin/* /usr/local/bin/; \
			sudo mv protoc3/include/* /usr/local/include/; \
			go install google.golang.org/protobuf/cmd/protoc-gen-go@latest ; \
			export PATH=$PATH:$GOPATH/bin; \
		else \
			echo "Protoc is installed on your system."; \
		fi \
	fi

################################################################################
#                                                                              #
#                                    QUALITY                                   #
#                                                                              #
################################################################################

.PHONY: format
format:
	@$(GOFMT) ./pkg/...

.PHONY: lint
lint:
	@golangci-lint run

################################################################################
#                                                                              #
#                                    TESTING                                   #
#                                                                              #
################################################################################

.PHONY: test
test: ## Start the application.
	@$(GOTEST) -v ./test/unit/... -failfast

.PHONY: test-integration
test-integration: ## Start the application.
	@$(GOTEST) -v ./test/integration/...

.PHONY: test-coverage
test-coverage: ## Start the application.
	@$(GOTEST) -v ./test/unit/..

.PHONY: test-report
test-report: test-coverage ## Start the application.
	@$(GOTOOL) -v ./...

################################################################################
#                                                                              #
#                                    HELPER                                    #
#                                                                              #
################################################################################

# Display a help screen with descriptions of all the targets.
# The grep command searches for lines in all makefiles
# (MAKEFILE_LIST) that match a certain pattern.
# The -h option tells grep not to print the names of the files
# where the matches were found.
# The -E option enables extended regular expressions, which
# allow more complex patterns.
#
# The regular expression '^[a-zA-Z_-]+:.*?## .*$$' matches lines
# that:
# - start with one or more alphanumeric characters or underscores
# or hyphens (^[a-zA-Z_-]+)
# - followed by a colon (:)
# - followed by any characters (.*?)
# - followed by '## '
# - followed by any characters (.*)
# - and end with the end of the line ($$)
#
# The awk command then splits each matching line into two parts
# at ':.*?## ' and prints them in a specific format.
# The printf command formats the output with the target name in
# cyan (\033[36m%-30s\033[0m) and the description in default
# color (%s\n).
.PHONY: help
help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+[.]*[a-zA-Z_-]+[.]*[a-zA-Z_-]*:.*?## .*$$' $(MAKEFILE_LIST) | \
	awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
