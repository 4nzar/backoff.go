package test_bench

import (
	"context"
	"errors"
	"math/rand"
	"testing"
	"time"

	backoff "gitlab.com/4nzar/backoff.go/pkg"
)

// Mock operation that fails with a certain probability
func mockOperation(failureProbability float64) func() error {
	return func() error {
		if rand.Float64() < failureProbability {
			return errors.New("operation failed")
		}
		return nil
	}
}

func BenchmarkBackOffRetry(b *testing.B) {
	config := backoff.NewConfig().
		SetInitialInterval(1 * time.Millisecond).
		SetMaxInterval(10 * time.Millisecond).
		SetMultiplier(1.5).
		SetMaxRetries(5).Build()
	executor := backoff.NewBackOff(config)
	// Context with sufficient timeout to allow multiple retries
	ctx, cancel := context.WithTimeout(context.Background(), 500*time.Millisecond)
	defer cancel()
	// Benchmark the retry logic with a high failure probability to ensure retries
	operation := mockOperation(0.9) // 90% failure rate
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		executor.Retry(ctx, operation)
	}
}
