package test_unit

import (
	"context"
	"errors"
	"testing"
	"time"

	backoff "gitlab.com/4nzar/backoff.go/pkg"
)

// Simulates different outcomes and interacts with the Context[T].
// func mockOperation[T any](shouldResolve bool, shouldFail bool) func(bctx *backoff.Context[T]) (T, error) {
// 	return func(bctx *backoff.Context[T]) (T, error) {
// 		bctx.RecordAttempt() // Record each retry attempt
// 		// Randomly decide if the operation will delay (simulate work or wait time)
// 		if rand.Intn(2) == 0 {
// 			bctx.Delay(100 * time.Millisecond)
// 		}
// 		if shouldResolve {
// 			// Simulate a situation where the operation succeeds
// 			var result T // Assuming T can be an int for this example
// 			return bctx.Resolve(result)
// 		}
// 		if shouldFail {
// 			// Simulate an error and reject the retry attempt
// 			return bctx.Reject(errors.New("operation failed"))
// 		}
// 		// Return a nil or zero value of T with no error, using Resolve to mark success
// 		var zero T
// 		return bctx.Resolve(zero)
// 	}
// }

func TestBackOff(t *testing.T) {
	// Setting up a common configuration for tests
	config := &backoff.Config{
		InitialInterval: 10 * time.Millisecond,
		MaxInterval:     50 * time.Millisecond,
		Multiplier:      1.5,
		MaxRetries:      5,
	}

	executor := backoff.NewBackOff(config)

	t.Run("Retry", func(t *testing.T) {
		t.Run("SuccessfulOperation", func(t *testing.T) {
			operation := func() error {
				return nil // Simulates a successful operation
			}

			err := executor.Retry(context.Background(), operation)
			if err != nil {
				t.Errorf("Expected no error for successful operation, got %v", err)
			}
		})

		t.Run("NoRetry", func(t *testing.T) {
			config := backoff.BackOffNoRetry()
			executor := backoff.NewBackOff(config)
			// Counts the number of retries
			var attempts uint8
			operation := func() error {
				if attempts < 7 {
					attempts++
					return errors.New("operation failed")
				}
				return nil
			}
			err := executor.Retry(context.Background(), operation)
			if err == nil {
				t.Error("Expected error after retries instead got nil")
			}
			if attempts != 1 {
				t.Errorf("Expected exactly 1 attempts, got %d", attempts)
			}
		})

		t.Run("FailWithRetries", func(t *testing.T) {
			// Counts the number of retries
			var attempts uint8
			operation := func() error {
				if attempts < 2 {
					attempts++
					return errors.New("operation failed")
				}
				return nil
			}

			err := executor.Retry(context.Background(), operation)
			if err != nil {
				t.Errorf("Expected no error after retries, got %v", err)
			}
			if attempts != 2 {
				t.Errorf("Expected exactly 2 attempts, got %d", attempts)
			}
		})

		t.Run("MaxRetriesExceeded", func(t *testing.T) {
			operation := func() error {
				return errors.New("operation failed")
			}

			err := executor.Retry(context.Background(), operation)
			if err == nil || err.Error() != "operation failed" {
				t.Errorf("Expected error 'operation failed' after max retries, got %v", err)
			}
		})

		t.Run("Cancellation", func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())
			cancel() // Cancel the context immediately

			operation := func() error {
				return errors.New("operation failed")
			}

			err := executor.Retry(ctx, operation)
			if err != context.Canceled {
				t.Errorf("Expected context.Canceled error, got %v", err)
			}
		})

		t.Run("ContextCancellationDuringDelay", func(t *testing.T) {
			// create a context that will cancel after a delay slightly shorter than the expected backoff delay.
			ctx, cancel := context.WithTimeout(context.Background(), 15*time.Millisecond)
			defer cancel()

			operation := func() error {
				// less than the context timeout but intended to simulate work
				time.Sleep(10 * time.Millisecond)
				return errors.New("operation should not complete")
			}

			startTime := time.Now()
			err := executor.Retry(ctx, operation)
			if err != context.DeadlineExceeded {
				t.Errorf("Expected context.DeadlineExceeded error, got %v", err)
			}
			if duration := time.Since(startTime); duration < 15*time.Millisecond || duration > 20*time.Millisecond {
				t.Errorf("Retry did not respect the context cancellation timing: expected between 15ms and 20ms, got %v", duration)
			}
		})
	})

}
