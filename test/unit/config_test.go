package test_unit

import (
	"testing"
	"time"

	backoff "gitlab.com/4nzar/backoff.go/pkg"
)

func TestConfigBuilder(t *testing.T) {
	t.Run("ConfigBuilder", func(t *testing.T) {
		builder := backoff.NewConfig()

		t.Run("SetInitialInterval", func(t *testing.T) {
			builder.SetInitialInterval(2 * time.Second)
			config := builder.Build()
			if config.InitialInterval != 2*time.Second {
				t.Errorf("SetInitialInterval failed: expected 2s, got %v", config.InitialInterval)
			}
		})

		t.Run("SetMaxInterval", func(t *testing.T) {
			builder.SetMaxInterval(5 * time.Minute)
			config := builder.Build()
			if config.MaxInterval != 5*time.Minute {
				t.Errorf("SetMaxInterval failed: expected 5m, got %v", config.MaxInterval)
			}
		})

		t.Run("SetMultiplier", func(t *testing.T) {
			builder.SetMultiplier(2.0)
			config := builder.Build()
			if config.Multiplier != 2.0 {
				t.Errorf("SetMultiplier failed: expected 2.0, got %v", config.Multiplier)
			}
		})

		t.Run("SetMaxRetries", func(t *testing.T) {
			builder.SetMaxRetries(5)
			config := builder.Build()
			if config.MaxRetries != 5 {
				t.Errorf("SetMaxRetries failed: expected 5, got %v", config.MaxRetries)
			}
		})

		t.Run("Build", func(t *testing.T) {
			config := builder.Build()
			if config == nil {
				t.Error("Build failed: config is nil")
			}
			// Additional checks could be added here if the config structure allowed for visibility
		})

		t.Run("BackOffNoRetry", func(t *testing.T) {
			noRetryConfig := backoff.BackOffNoRetry()
			if noRetryConfig.MaxRetries != 0 {
				t.Errorf("BackOffNoRetry failed: expected MaxRetries = 0, got %d", noRetryConfig.MaxRetries)
			}
		})
	})
}
