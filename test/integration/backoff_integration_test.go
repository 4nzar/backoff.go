package test_integration

import (
	"context"
	"errors"
	"sync"
	"testing"
	"time"

	backoff "gitlab.com/4nzar/backoff.go/pkg"
)

func TestBackOffExecutorConcurrency(t *testing.T) {
	config := backoff.NewConfig().
		SetInitialInterval(100 * time.Millisecond). // Extended to ensure noticeable delays
		SetMaxInterval(300 * time.Millisecond).     // Ensures a significant delay on each retry
		SetMultiplier(2.0).                         // Increases the delay more significantly with each retry
		SetMaxRetries(5).                           // Allows multiple retries
		Build()
	executor := backoff.NewBackOff(config)
	// Operation with induced delay to increase chance of hitting context timeout
	operation := func() error {
		time.Sleep(50 * time.Millisecond) // Simulate work that takes time
		return errors.New("operation consistently fails")
	}
	// Reduced context timeout to make exceeding it more likely
	ctx, cancel := context.WithTimeout(context.Background(), 500*time.Millisecond)
	defer cancel()
	const goroutineCount = 10
	var wg sync.WaitGroup
	wg.Add(goroutineCount)
	errorsEncountered := make([]error, 0, goroutineCount)
	mutex := sync.Mutex{}

	for i := 0; i < goroutineCount; i++ {
		go func() {
			defer wg.Done()
			err := executor.Retry(ctx, operation)
			mutex.Lock()
			errorsEncountered = append(errorsEncountered, err)
			mutex.Unlock()
		}()
	}
	wg.Wait()
	// Check if the context deadline was expected to be exceeded
	for _, err := range errorsEncountered {
		if err != nil && err != context.DeadlineExceeded {
			t.Errorf("Operation failed or context exceeded: %v", err)
		}
	}
	// Confirm that the context's deadline was indeed exceeded
	if ctx.Err() != context.DeadlineExceeded {
		t.Errorf("Expected context deadline to be exceeded, got: %v", ctx.Err())
	}
}
