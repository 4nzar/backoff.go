package backoff

import (
	"context"
	"math"
	"math/rand"
	"sync"
	"time"
)

// references:
// - https://aws.amazon.com/blogs/architecture/exponential-backoff-and-jitter/
// - https://stackoverflow.com/questions/46939285/why-is-random-jitter-applied-to-back-off-strategies

type BackOffExecutor struct {
	config *Config
	mutex  sync.Mutex
}

func NewBackOff(config *Config) *BackOffExecutor {
	return &BackOffExecutor{
		config: config,
	}
}

// Retry method now uses just an error from the operation function.
func (b *BackOffExecutor) Retry(ctx context.Context, operation func() error) error {
	retryCount := uint8(0)
	for {
		// Prioritize checking the context's cancellation status before any operation
		if err := ctx.Err(); err != nil {
			return err // Handle cancellation or timeout immediately
		}
		err := operation()
		if err == nil {
			return nil // If no error, operation succeeded
		}
		// Check if retries should continue
		retryCount++
		if retryCount >= b.config.MaxRetries {
			return err // Return the last error encountered after retries are exhausted
		}
		// Calculate delay with exponential backoff and jitter
		delay := calculateDelay(b.config, retryCount)
		// Wait for the delay duration or until the context is cancelled
		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-time.After(delay):
			continue
		}
	}
}

func calculateDelay(config *Config, retryCount uint8) time.Duration {
	delay := time.Duration(float64(config.InitialInterval) * math.Pow(float64(config.Multiplier), float64(retryCount-1)))
	if delay > config.MaxInterval {
		delay = config.MaxInterval
	}
	jitter := time.Duration(rand.Float64() * float64(delay))
	return delay + jitter
}
