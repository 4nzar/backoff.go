package backoff

import (
	"sync"
	"time"
)

const (
	DefaultInitialInterval = 500 * time.Millisecond
	DefaultMultiplier      = 1.5
	DefaultMaxInterval     = 60 * time.Second
	DefaultMaxRetries      = 10
)

type IBackOffConfig interface {
	SetInitialInterval(interval time.Duration) IBackOffConfig
	SetMaxInterval(interval time.Duration) IBackOffConfig
	SetMultiplier(multiplier float32) IBackOffConfig
	SetMaxRetries(retries uint8) IBackOffConfig
	NoRetryPolicy() IBackOffConfig
	Build() IBackOffConfig
}

// Config holds configuration for the backoff strategy.
type Config struct {
	InitialInterval time.Duration `json:"initial_interval"`
	MaxInterval     time.Duration `json:"max_interval"`
	Multiplier      float32       `json:"multiplier"`
	MaxRetries      uint8         `json:"max_retries"`
}

// Builder is responsible for constructing a backoff configuration.
type ConfigBuilder struct {
	config    *Config
	usedFlags struct {
		initialInterval bool
		maxInterval     bool
		multiplier      bool
		maxRetries      bool
	}
	mutex sync.Mutex
}

// NewBuilder returns a new instance of the Builder with default values.
func NewConfig() *ConfigBuilder {
	return &ConfigBuilder{
		config: &Config{
			InitialInterval: DefaultInitialInterval,
			MaxInterval:     DefaultMaxInterval,
			Multiplier:      DefaultMultiplier,
			MaxRetries:      DefaultMaxRetries,
		},
	}
}

// SetInitialInterval sets the initial interval for the backoff strategy.
func (instance *ConfigBuilder) SetInitialInterval(interval time.Duration) *ConfigBuilder {
	instance.mutex.Lock()
	defer instance.mutex.Unlock()
	if !instance.usedFlags.initialInterval {
		instance.config.InitialInterval = interval
		instance.usedFlags.initialInterval = true
	}
	return instance
}

// SetMaxInterval sets the maximum interval for the backoff strategy.
func (instance *ConfigBuilder) SetMaxInterval(interval time.Duration) *ConfigBuilder {
	instance.mutex.Lock()
	defer instance.mutex.Unlock()
	if !instance.usedFlags.maxInterval {
		instance.config.MaxInterval = interval
		instance.usedFlags.maxInterval = true
	}
	return instance
}

// SetMultiplier sets the multiplier for the interval increase after each retry.
func (instance *ConfigBuilder) SetMultiplier(multiplier float32) *ConfigBuilder {
	instance.mutex.Lock()
	defer instance.mutex.Unlock()
	if !instance.usedFlags.multiplier {
		instance.config.Multiplier = multiplier
		instance.usedFlags.multiplier = true
	}
	return instance
}

// SetMaxRetries sets the maximum number of retry attempts.
func (instance *ConfigBuilder) SetMaxRetries(retries uint8) *ConfigBuilder {
	instance.mutex.Lock()
	defer instance.mutex.Unlock()
	if !instance.usedFlags.maxRetries {
		instance.config.MaxRetries = retries
		instance.usedFlags.maxRetries = true
	}
	return instance
}

// Build finalizes the configuration and prevents any further modifications.
func (instance *ConfigBuilder) Build() *Config {
	return instance.config
}

func BackOffNoRetry() *Config {
	return &Config{
		InitialInterval: DefaultInitialInterval,
		MaxInterval:     DefaultMaxInterval,
		Multiplier:      DefaultMultiplier,
		MaxRetries:      0,
	}
}
