package backoff_interface

import (
	"time"
)

type IBackOffConfig interface {
	SetInitialInterval(interval time.Duration) IBackOffConfig
	SetMaxInterval(interval time.Duration) IBackOffConfig
	SetMultiplier(multiplier float32) IBackOffConfig
	SetMaxRetries(retries uint8) IBackOffConfig
	NoRetryPolicy() IBackOffConfig
	Build() IBackOffConfig
}
