package backoff_interface

import (
	"context"
	"time"
)

type IBackOffExecutor interface {
	Notify(notifyFn func(error, time.Duration) error) IBackOffExecutor
	Retry(ctx context.Context, operation func() error) error
}
