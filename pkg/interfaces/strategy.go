package backoff_interface

import "time"

// IStrategy defines the interface for backoff strategies.
type IStrategy interface {
	NextBackOff() time.Duration
	Reset()
}
